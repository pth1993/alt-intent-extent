# Alt Intent Detector Document
-----------------------------------------------------------------
Code by **Thai-Hoang Pham** at Alt Inc.

A demo website is available at [nnvlp.org](http://nnvlp.org)

## Usage
To train, eval, and export models, run script ``export_model.py``. An example:
```sh
	$ python export_model.py --vectorizer tfidf --classifier SVM --label Emotion
```
For more details, run:
```sh
	$ python export_model.py -h
```

To use as a service, run script ``service.py``. An example:
```sh
	$ python service.py --vectorizer tfidf --classifier SVM --port 1999
```
For more details, run:
```sh
	$ python service.py -h
```

## Contact

**Thai-Hoang Pham** < phamthaihoang.hn@gmail.com >

Alt Inc, Hanoi, Vietnam
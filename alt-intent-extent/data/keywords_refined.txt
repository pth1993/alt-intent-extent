Conjecture: だろ,確か,きっと,じゃ,長い,しか,でしょ,らしい,たぶん
Confirmation: 嘘,かなり,べき,なるほど,待ち合わせ,一体,やはり
Correction: いいえ,実は,いや,いえ,じゃ
Disagreement: おしゃれ,やら,無理,決して,ギレ,未満,ううん,いいえ,せつない,そんな,なんて,まったく,違い,欠点,いえ,まだまだ
Agreement: 分かり,思い,わかり,大好き,とても,ほんと,うん,たしかに,確か,もちろん,そうですね,ええ,はい,そう
Promise: かならず,今後,必ず,がんばっ,いつか,ましょ,もし,ゴメン,約束
Suggest: やめ,ましょ,オススメ,止め,ください,下さい,どう,いかが,ぜひ,おすすめ,ほう,早め,よい
Request: やめ,もらえ,おすすめ,止め,しまう,ください,お願い,下さい,預かっ,教え,くれ

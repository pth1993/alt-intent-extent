import codecs


cate_list = []
keyword_list = []
with codecs.open('../data/keywords_refined.txt', 'r', 'utf-8') as f:
    for line in f:
        line = line.strip().split()
        cate = line[0].replace(u':', u'')
        keyword = line[1].split(u',')
        cate_list.append(cate)
        keyword_list.append(keyword)
# non_keyword_list = []
# for i in range(len(keyword_list)):
#     non_keyword = keyword_list[:i] + keyword_list[(i+1):]
#     non_keyword_list.append(list(set([item for sublist in non_keyword for item in sublist])))


with codecs.open('../data/twitter_data.txt', 'r', 'utf-8') as f1, codecs.open('../data/new_data.txt', 'w', 'utf-8') as f2:
    for line in f1:
        keyword_mark_list = [0] * len(keyword_list)
        line = line.strip().split()
        for word in line:
            for i in range(len(keyword_list)):
                if word in keyword_list[i]:
                    cate = cate_list[i]
                    keyword_mark_list[i] = 1
        if sum(keyword_mark_list) == 1:
            f2.write(' '.join(line) + u'\t' + cate + u'\n')

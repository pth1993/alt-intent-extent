import codecs
import collections
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import preprocessing
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import f1_score
import cPickle as pickle


def read_data(data_file):
    sent = []
    label = []
    with codecs.open(data_file, 'r', 'utf-8') as f:
        for line in f:
            line = line.strip().split('\t')
            sent.append(line[0])
            label.append(line[1])
    return sent, label


def data_statistic(label, name):
    print 'Data Statistics %s:' % name
    print 'Label: ' + str(dict(collections.Counter(label)))


def vector_transform(data_train, data_test, label_type, method):
    if method == 'bow':
        vectorizer = CountVectorizer(token_pattern=r'(?u)\b\w\w+\b', min_df=1, binary=False)
    elif method == 'tfidf':
        vectorizer = TfidfVectorizer(token_pattern=r'(?u)\b\w\w+\b', min_df=1)
    else:
        raise ValueError("Method should choose from [bow, tfidf]")
    vectorizer.fit_transform(data_train + data_test)
    X_train = vectorizer.transform(data_train)
    X_test = vectorizer.transform(data_test)
    with open('../model/vectorizer/' + label_type.lower() + '_' + method + '.pkl', 'wb') as output:
        pickle.dump(vectorizer, output, pickle.HIGHEST_PROTOCOL)
    return X_train, X_test


def string_2_index(data_train, data_test, label_type):
    le = preprocessing.LabelEncoder()
    le.fit(data_train + data_test)
    X_train = le.transform(data_train)
    X_test = le.transform(data_test)
    with open('../model/label_encoder/' + label_type.lower() + '_label_encoder.pkl', 'wb') as output:
        pickle.dump(le, output, pickle.HIGHEST_PROTOCOL)
    return X_train, X_test, le


def train(vector_train, vector_test, label_train, vectorizer, label_type, method):
    if method == 'SVM':
        clf_label = LinearSVC(loss='squared_hinge', penalty="l2", dual=False, tol=1e-3)
    elif method == 'LR':
        clf_label = LogisticRegression()
    elif method == 'NB':
        clf_label = MultinomialNB()
    elif method == 'DT':
        clf_label = DecisionTreeClassifier()
    else:
        raise ValueError("Method should choose from [SVM, LR, NB, DT]")
    clf_label.fit(vector_train, label_train)
    label_train_predict = clf_label.predict(vector_train)
    label_test_predict = clf_label.predict(vector_test)
    with open('../model/model/' + label_type.lower() + '_' + method.lower() + '_' + vectorizer.lower() + '.pkl', 'wb') as output:
        pickle.dump(clf_label, output, pickle.HIGHEST_PROTOCOL)
    return label_train_predict, label_test_predict


def f1_eval(label_train_predict, label_test_predict, label_train, label_test, label_encoder, method):
    if method not in ['binary', 'micro', 'macro', 'samples', 'weighted']:
        raise ValueError("Method should choose from binary, micro, macro, samples, weighted]")
    labels = label_encoder.classes_
    print 'Training data:'
    print 'F1 score total: %2f' % (f1_score(label_train, label_train_predict, average=method))
    f1_train = f1_score(label_train, label_train_predict, average=None, labels=range(len(labels)))
    for i in range(len(labels)):
        print 'F1 score %s: %2f' % (labels[i], f1_train[i])
    print 'Testing data:'
    print 'F1 score total: %2f' % (f1_score(label_test, label_test_predict, average=method))
    f1_test = f1_score(label_test, label_test_predict, average=None, labels=range(len(labels)))
    for i in range(len(labels)):
        print 'F1 score %s: %2f' % (labels[i], f1_test[i])


def read_extent_data(data_file, label_type):
    sent = []
    label = []
    with codecs.open(data_file, 'r', 'utf-8') as f:
        for line in f:
            line = line.strip().split('\t')
            sent.append(line[0])
            if label_type == 'Domain':
                label.append(line[1])
            elif label_type == 'Objective':
                label.append(line[2])
            elif label_type == 'Emotion':
                label.append(line[3])
    return sent, label
import codecs
import itertools
from sklearn.model_selection import train_test_split

sent_list = []
domain_list = []
objective_list = []
emotion_list = []
with codecs.open('../data/intent_data.txt', 'r', 'utf8') as f:
    for line in f:
        line = line.strip().split(u'\t')
        sent_list.append(line[0])
        domain_list.append(line[1])
        objective_list.append(line[2])
        emotion_list.append(line[3])
domain_set = list(set(domain_list))
objective_set = list(set(objective_list))
emotion_set = list(set(emotion_list))
domain_index_list = [[] for item in domain_set]
objective_index_list = [[] for item in objective_set]
emotion_index_list = [[] for item in emotion_set]
index = 0
for domain, objective, emotion in itertools.izip(domain_list, objective_list, emotion_list):
    index += 1
    domain_index = domain_set.index(domain)
    domain_index_list[domain_index].append(index)
    objective_index = objective_set.index(objective)
    objective_index_list[objective_index].append(index)
    emotion_index = emotion_set.index(emotion)
    emotion_index_list[emotion_index].append(index)
domain_index_train = []
domain_index_test = []
for domain_index in domain_index_list:
    train, test = train_test_split(domain_index, test_size=0.3, random_state=42)
    domain_index_train += train
    domain_index_test += test
domain_index_train.sort()
domain_index_test.sort()
objective_index_train = []
objective_index_test = []
for objective_index in objective_index_list:
    train, test = train_test_split(objective_index, test_size=0.3, random_state=42)
    objective_index_train += train
    objective_index_test += test
objective_index_train.sort()
objective_index_test.sort()
emotion_index_train = []
emotion_index_test = []
for emotion_index in emotion_index_list:
    train, test = train_test_split(emotion_index, test_size=0.3, random_state=42)
    emotion_index_train += train
    emotion_index_test += test
emotion_index_train.sort()
emotion_index_test.sort()
with codecs.open('../data/domain_train.txt', 'w', 'utf-8') as f1, codecs.open('../data/domain_test.txt', 'w', 'utf-8') as f2:
    i = 0
    j = 0
    for sent, domain in itertools.izip(sent_list, domain_list):
        if domain_index_train[i] < domain_index_test[j]:
            f1.write(sent + u'\t' + domain + u'\n')
            i += 1
            if i == len(domain_index_train):
                domain_index_train.append(100000)
        elif domain_index_train[i] > domain_index_test[j]:
            f2.write(sent + u'\t' + domain + u'\n')
            j += 1
            if j == len(domain_index_test):
                domain_index_test.append(100000)
with codecs.open('../data/objective_train.txt', 'w', 'utf-8') as f1, codecs.open('../data/objective_test.txt', 'w', 'utf-8') as f2:
    i = 0
    j = 0
    for sent, objective in itertools.izip(sent_list, objective_list):
        if objective_index_train[i] < objective_index_test[j]:
            f1.write(sent + u'\t' + objective + u'\n')
            i += 1
            if i == len(objective_index_train):
                objective_index_train.append(100000)
        elif objective_index_train[i] > objective_index_test[j]:
            f2.write(sent + u'\t' + objective + u'\n')
            j += 1
            if j == len(objective_index_test):
                objective_index_test.append(100000)
with codecs.open('../data/emotion_train.txt', 'w', 'utf-8') as f1, codecs.open('../data/emotion_test.txt', 'w', 'utf-8') as f2:
    i = 0
    j = 0
    for sent, emotion in itertools.izip(sent_list, emotion_list):
        if emotion_index_train[i] < emotion_index_test[j]:
            f1.write(sent + u'\t' + emotion + u'\n')
            i += 1
            if i == len(emotion_index_train):
                emotion_index_train.append(100000)
        elif emotion_index_train[i] > emotion_index_test[j]:
            f2.write(sent + u'\t' + emotion + u'\n')
            j += 1
            if j == len(emotion_index_test):
                emotion_index_test.append(100000)

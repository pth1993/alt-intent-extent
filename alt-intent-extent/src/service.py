from flask import Flask, request, jsonify
from flask_cors import CORS
import os
import argparse
import cPickle as pickle
import MeCab
tagger = MeCab.Tagger("-Owakati")

app = Flask(__name__)
cors = CORS(app)

parser = argparse.ArgumentParser()
parser.add_argument("--vectorizer", help="choose between [bow, tfidf]")
parser.add_argument("--classifier", help="choose among [SVM, LR, NB, DT]")
parser.add_argument("--port", help="port number")

args = parser.parse_args()
vectorizer = args.vectorizer
classifier = args.classifier
port = int(args.port)


with open('../model/label_encoder/domain_label_encoder.pkl', 'rb') as handle:
    le_domain = pickle.load(handle)
with open('../model/label_encoder/objective_label_encoder.pkl', 'rb') as handle:
    le_objective = pickle.load(handle)
with open('../model/label_encoder/emotion_label_encoder.pkl', 'rb') as handle:
    le_emotion = pickle.load(handle)
with open('../model/model/domain_' + classifier + '_' + vectorizer + '.pkl', 'rb') as handle:
    domain_model = pickle.load(handle)
with open('../model/model/objective_' + classifier + '_' + vectorizer + '.pkl', 'rb') as handle:
    objective_model = pickle.load(handle)
with open('../model/model/emotion_' + classifier + '_' + vectorizer + '.pkl', 'rb') as handle:
    emotion_model = pickle.load(handle)
with open('../model/vectorizer/domain_' + vectorizer + '.pkl', 'rb') as handle:
    domain_vectorizer = pickle.load(handle)
with open('../model/vectorizer/objective_' + vectorizer + '.pkl', 'rb') as handle:
    objective_vectorizer = pickle.load(handle)
with open('../model/vectorizer/emotion_' + vectorizer + '.pkl', 'rb') as handle:
    emotion_vectorizer = pickle.load(handle)


def tokenize(raw_sentence):
    result = tagger.parse(raw_sentence)
    words = result.split()
    if len(words) == 0:
        return ""
    if words[-1] == "\n":
        words = words[:-1]
    return words


def detector(sent):
    # token_sent = tokenize(sent)
    # token_sent = [unicode(i, 'utf-8') for i in token_sent]
    domain_vector_sent = domain_vectorizer.transform([sent])
    objective_vector_sent = objective_vectorizer.transform([sent])
    emotion_vector_sent = emotion_vectorizer.transform([sent])
    domain = domain_model.predict(domain_vector_sent)
    objective = objective_model.predict(objective_vector_sent)
    emotion = emotion_model.predict(emotion_vector_sent)
    domain = le_domain.inverse_transform(domain)[0]
    objective = le_objective.inverse_transform(objective)[0]
    emotion = le_emotion.inverse_transform(emotion)[0]
    return domain, objective, emotion


@app.route('/intent', methods=['POST'])
def intent_json():
    data = request.get_json()
    text = data['text']
    domain, objective, emotion = detector(text)
    output = dict()
    output['domain'] = domain
    output['objective'] = objective
    output['emotion'] = emotion
    return jsonify(output)


@app.route("/")
def hello():
    return "<h1>This is a English Alt Intent Detector</h1>"


if __name__ == "__main__":
    host = os.environ.get('IP', '0.0.0.0')
    port = int(os.environ.get('PORT', port))
    app.run(host=host, port=port)

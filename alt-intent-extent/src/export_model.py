import argparse
import utils
import warnings
warnings.filterwarnings("ignore")


parser = argparse.ArgumentParser()
parser.add_argument("--vectorizer", help="choose between [bow, tfidf]")
parser.add_argument("--classifier", help="choose among [SVM, LR, NB, DT]")
parser.add_argument("--use_twitter", action='store_true', help="use Twitter data for Objective")
parser.add_argument("--label", help="choose among [Domain, Objective, Emotion]")
parser.add_argument("--extent", action='store_false', help="use extent data")

args = parser.parse_args()

label_type = args.label
use_twitter = args.use_twitter
vectorizer = args.vectorizer
classifier = args.classifier
extent = args.extent


def main():
    print 'Loading data...'
    if label_type == 'Domain':
        sent_train, label_train = utils.read_data('../data/domain_train.txt')
        sent_test, label_test = utils.read_data('../data/domain_test.txt')
    elif label_type == 'Objective':
        sent_train, label_train = utils.read_data('../data/objective_train.txt')
        sent_test, label_test = utils.read_data('../data/objective_test.txt')
    elif label_type == 'Emotion':
        sent_train, label_train = utils.read_data('../data/emotion_train.txt')
        sent_test, label_test = utils.read_data('../data/emotion_test.txt')
    sent_extent, label_extent = utils.read_extent_data('../data/intent_data_extent.txt', label_type)
    print 'Data statistics'
    utils.data_statistic(label_train, 'Train')
    utils.data_statistic(label_test, 'Test')
    if extent:
        sent_train += sent_extent
        label_train += label_extent
    if label_type == 'Objective' and use_twitter:
        sent_twitter, label_twitter = utils.read_data('../data/new_data.txt')
        sent_train += sent_twitter
        label_train += label_twitter
    print 'Transforming data...'
    vector_train, vector_test = utils.vector_transform(sent_train, sent_test, label_type, method=vectorizer)
    label_train_index, label_test_index, label_encoder = utils.string_2_index(label_train, label_test, label_type)
    print 'Training...'
    label_train_predict, label_test_predict = utils.train(vector_train, vector_test, label_train_index, vectorizer,
                                                          label_type, method=classifier)
    print 'Evaluating...'
    utils.f1_eval(label_train_predict, label_test_predict, label_train_index, label_test_index, label_encoder,
                  method='weighted')


if __name__ == '__main__':
    main()

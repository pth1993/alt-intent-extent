import codecs
import math
import itertools
import numpy as np
import heapq
from collections import Counter


with codecs.open('../data/intent_data.txt', 'r', 'utf-8') as f:
    # domain = []
    # objective = []
    # emotion = []
    objective_list = ['Conjecture', 'Confirmation', 'Offer', 'Question', 'Correction', 'Disagreement', 'Agreement',
                      'Informative', 'Other', 'Promise', 'Suggest', 'Request']
    corpus = dict()
    for line in f:
        line = line.strip().split('\t')
        # domain.append(line[1])
        # objective.append(line[2])
        # emotion.append(line[3])
        sent = line[0].split()
        for objective in objective_list:
            if line[2] == objective:
                try:
                    corpus[objective] += sent
                except:
                    corpus[objective] = sent
corpus_stat = dict()
for key, value in corpus.iteritems():
    corpus_stat[key] = dict(Counter(value))
corpus_key = corpus_stat.keys()
corpus_value = corpus_stat.values()
corpus_tf = []
corpus_idf = []
corpus_word = []
for item in corpus_value:
    word = item.keys()
    freq = item.values()
    max_freq = float(max(freq))
    corpus_tf.append([i/max_freq for i in freq])
    corpus_word.append(word)
for i in range(len(corpus_word)):
    source = corpus_word[i]
    targets = corpus_word[:i] + corpus_word[(i+1):]
    idf = []
    for word in source:
        cnt = 1
        for target in targets:
            if word in target:
                cnt += 1
        # print cnt
        idf.append(math.log(12/float(cnt)))
    corpus_idf.append(idf)
corpus_tfidf = []
for tf, idf in itertools.izip(corpus_tf, corpus_idf):
    prod = list(np.multiply(tf, idf))
    corpus_tfidf.append(prod)
thresholds = [min(heapq.nlargest(30, i)) for i in corpus_tfidf]
corpus_keyword = []
for words, values, threshold in itertools.izip(corpus_word, corpus_tfidf, thresholds):
    keyword = []
    for word, value in itertools.izip(words, values):
        if value >= threshold:
            keyword.append(word)
    corpus_keyword.append(keyword)
with codecs.open('../data/keywords.txt', 'w', 'utf-8') as f:
    for i in range(len(corpus_key)):
        f.write(corpus_key[i] + ': ' + ','.join(corpus_keyword[i]) + '\n')
# for item in corpus_keyword[10]:
#     print item
# print corpus_key




